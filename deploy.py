#!/usr/bin/env python3

import os
import argparse
import mimetypes

import boto3

parser = argparse.ArgumentParser()
parser.add_argument('-d', '--delete', action='store_true')
parser.add_argument('-p', '--production', action='store_true')
args = parser.parse_args()

branch = os.environ['CI_COMMIT_REF_NAME']
project_name = os.environ['CI_PROJECT_NAME']
project_namespace = os.environ['CI_PROJECT_NAMESPACE']

assets_dir = 'public'
assets_dir_len = len(assets_dir) + 1

s3 = boto3.resource('s3')
client = boto3.client('s3')

bucket_name = "%s-%s-%s" % (branch, project_name, project_namespace)

def delete_bucket():
    bucket = s3.Bucket(bucket_name)
    objects_to_delete = []
    for obj in bucket.objects.all():
        objects_to_delete.append({'Key': obj.key})
    bucket.delete_objects(Delete={'Objects': objects_to_delete})
    bucket.delete()

def deploy_bucket():
    print("Deploying %s" % bucket_name)

    if not s3.Bucket(bucket_name) in s3.buckets.all():
        client.create_bucket(Bucket=bucket_name)

    response = client.put_bucket_website(
        Bucket=bucket_name,
        WebsiteConfiguration={
            'ErrorDocument': {
                'Key': 'error.html'
            },
            'IndexDocument': {
                'Suffix': 'index.html'
            },
        }
    )

    for root, _, filenames in os.walk('public'):
        for filename in filenames:
            file = os.path.join(root, filename)
            type = mimetypes.guess_type(file)[0]
            client.upload_file(
                file,
                bucket_name,
                file[assets_dir_len:],
                {'ACL': 'public-read', 'ContentType': type}
            )

if args.delete:
    delete_bucket()
    exit(0)
elif args.production:
    bucket_name = "%s-%s" % (project_name, project_namespace)

deploy_bucket()
